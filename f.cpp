#include "f.h"
#include <time.h>



void wait ( int time )
{
  /*
  float a=0.1;
  float b=0.1;
  float c;
  for (long int i=0;i!=(HHERZ/3)*seconds;++i) c=a*b;
  */
         struct timespec reqtime;        reqtime.tv_sec = time/SECOND_PART;
        reqtime.tv_nsec = (1000000000/SECOND_PART) * (time%SECOND_PART);
        nanosleep(&reqtime, NULL);
}


double f(double* x, int time) {
    wait(time);
    return x[0]*x[0]+x[1]*x[1]+x[2]*x[2];
}
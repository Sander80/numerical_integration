#include "f.h"
#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include <mpi.h>
#include <time.h>
#include <sys/time.h>

//int64_t M = 1;
int n = 3;
int step = 1;
int k;

void evaluate(int64_t M, int time) {

  timeval start_timeA,stop_timeA;
  gettimeofday(&start_timeA,NULL);		
 
  double res = 0.;
  
  double* x = (double*)  malloc(n*sizeof(double));
  double* shift = (double*)   malloc(n*sizeof(double));
  int64_t* count = (int64_t*) malloc(n*sizeof(int64_t));
  
  int64_t common = floor(pow(M,1./n));
  int64_t remain = ceil(M/pow(floor(pow(M,1./n)),n-1));
  int64_t M_corrected = pow(common,n-1)*remain;
  
  for (int i = 0; i != n; i++) if (i!=n-1) shift[i]=1./common; else shift[i]=1./remain;  
  

  for (int i = 0; i != n; i++) x[i]=shift[i]/2;
  for (int i = 0; i != n; i++) count[i]=0;
  x[0]-=(step-k)*shift[0];
  count[0]=-(step-k);
  
  for (int64_t i = k; i < M_corrected; i+=step) { // different MPI branches will start from values from 0 to step-1
      int local_step = step;
      
      for (int j = 0; j!=n; j++) {			
	    
	count[j]+=local_step;
	if ((j!=n-1) && count[j]>=common) { // we do not need overflow check for the last one	    
	    local_step = count[j]/common; // whole part, how many full common values are in count[j]; it is at least 1 and it is 1 in step=1
	    count[j]=count[j]%common;	// the remainder; for step=1 it will be set to 0;
	    x[j]=shift[j]/2 + (count[j]*shift[j]);
	} else {
	    x[j]+= local_step*shift[j];	    
	    break;
	}
      }	//for j
    
    //  printf("{%ld,%f,%f,%f}\n",i,x[0],x[1],x[2]);	
      res+=(f(x,time)/M_corrected); 
      
    
  } //for i
  
  
  free(x);
  free(shift);
  free(count);
    
  //printf("%f\n",res);
    
  double total;
  MPI_Reduce(&res, &total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
  
  
  

  gettimeofday(&stop_timeA,NULL);

  float ttime = ((float)(stop_timeA.tv_usec-start_timeA.tv_usec)/1000000)+(float)(stop_timeA.tv_sec-start_timeA.tv_sec);
  
  
  if(k==0) {
    printf("%d %ld %f %f %f %f %f\n",step,M_corrected,(float)M_corrected/(ttime),(float)M_corrected*time/(ttime*step*SECOND_PART),(float) ttime,1-total,(float)time/SECOND_PART);      
    // processes, points, performance (calculations per second), effectiveness, time, error, time for single point 
    fflush(stdout);
  }
   
  
}


int main(int argc, char *argv[])
{
  
  if (argc<2) {
     printf("Usage: num_test <number of points> (can be mupltiple, time is fixed at 1/%d seconds) or num_test N <number of points> <time> measured in 0.0001 sec, can be multiple\n",SECOND_PART);
    return -1;
  }

  int requested=MPI_THREAD_FUNNELED;
  int provided;
  MPI_Init_thread(&argc, &argv, requested, &provided);

  MPI_Comm_size(MPI_COMM_WORLD,&step);
  
  MPI_Comm_rank(MPI_COMM_WORLD,&k);
  
  if (strcmp(argv[1],"N")) {
    
    // different number of points
  
    for (int arg_count=1;arg_count!=argc;arg_count++) { 
      int64_t M;
      sscanf(argv[arg_count],"%ld",&M); 
      evaluate(M,1000);	 
    }  
    
  } else {
    // same number of points, different evaluation time
    
    int64_t M;
    sscanf(argv[2],"%ld",&M); 
    
    for (int arg_count=3;arg_count<argc;arg_count++) { 
      int time;
      sscanf(argv[arg_count],"%d",&time); 
      evaluate(M,time);	 
    }  
    
  }
  
  MPI_Finalize();
  return 0;
  
}
num_test : main.o f.o
	mpicxx main.o f.o -Wl,-Bdynamic -lc -o $@

main.o: main.cpp
	mpicxx -O0 -g -I../usr/include/ -c $<

f.o: f.cpp
	mpicxx -O0 -g -I../usr/include/ -c $<


clean:
	rm -f main.o f.o num_test

